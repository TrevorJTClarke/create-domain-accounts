const { KeyPair, keyStores } = require('near-api-js');
const path = require('path');
const homedir = require('os').homedir();

const CREDENTIALS_DIR = '.near-credentials';
const credentialsPath = path.join(homedir, CREDENTIALS_DIR);

// alias top level account
const genNewAccount = async (accountId, nearConfig) => {
  const keyStore = new keyStores.UnencryptedFileSystemKeyStore(credentialsPath);

  const existingKey = await keyStore.getKey(nearConfig.networkId, accountId);
  if (existingKey) {
    console.log(`Key pair with ${existingKey.publicKey} public key for an account "${accountId}"`);
    return existingKey.publicKey;
  }

  const keyPair = KeyPair.fromRandom('ed25519');
  const publicKey = keyPair.publicKey.toString();
  const id = accountId || implicitAccountId(publicKey);
  await keyStore.setKey(nearConfig.networkId, id, keyPair);
  console.log(`Key pair with ${publicKey} public key for an account "${id}"`);
  return publicKey;
}

;(async () => {
  genNewAccount('Test1234allDayyyyyyyy', { networkId: 'testnet' })
})();
