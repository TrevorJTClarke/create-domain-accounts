const { connect, keyStores, utils, KeyPair } = require('near-api-js');
const path = require('path');
const homedir = require('os').homedir();

const CREDENTIALS_DIR = '.near-credentials';
const NETWORK_ID = process.env.NEAR_NETWORK || 'testnet'

// 1. user1.testnet create key with private key ed25519: HIIYOU…
// 2. and transfers another.testnet 100 Ⓝ ...does some other transactions, whatever,
// 3. user1.testnet then deletes the key, re - adds it with same private key, which resets the nonce, and
// 4. immediately another.testnet(who had captured the signed transaction) replays it, getting another 100 Ⓝ

// Code flow:
// 1. create new keypair
// 2. add key to existing accountId
// 3. send 1 Ⓝ using new keypair
// 4. store signed txn, keep keypair locally
// 5. send txn to chain 
// 6. delete keypair
// 7. add keypair back
// 8. send txn again 
// 9. validate, then repeat starting at #6


// const nearConfig = {
//   networkId: 'mainnet',
//   nodeUrl: 'https://rpc.mainnet.near.org',
//   walletUrl: 'https://wallet.mainnet.near.org',
//   helperUrl: 'https://helper.mainnet.near.org'
// };

const nearConfig = {
  networkId: 'testnet',
  networkName: 'testnet',
  nodeUrl: 'https://rpc.testnet.near.org',
  walletUrl: 'https://wallet.testnet.near.org',
  helperUrl: 'https://helper.testnet.near.org'
};

const ACCOUNT_A = 'pa.testnet'
const ACCOUNT_B = 'ion.testnet'

// begin: async function() {
//   const credentialsPath = path.join(homedir, CREDENTIALS_DIR);
//   const keyStore = new keyStores.UnencryptedFileSystemKeyStore(credentialsPath);
//   const near = await connect(Object.assign({ deps: { keyStore } }, nearConfig));
//   const creatorAccount = await near.account(process.argv[2]);

//   return await creatorAccount.functionCall(
//     'near',
//     'create_account',
//     {
//       new_account_id: process.argv[3],
//       new_public_key: process.argv[4]
//     },
//     '300000000000000',
//     utils.format.parseNearAmount('3')
//   )
// }


// Load environment variables
require("dotenv").config();

// Load NEAR components
const near = require("near-api-js");
const { sha256 } = require("js-sha256");
const fs = require("fs");

// Formatter helper for Near amounts
function formatAmount(amount) {
  return BigInt(near.utils.format.parseNearAmount(amount.toString()));
};

// Directory where Near credentials are going to be stored
const credentialsPath = path.join(homedir, CREDENTIALS_DIR);

// Configure the keyStore to be used with the SDK
const UnencryptedFileSystemKeyStore = near.keyStores.UnencryptedFileSystemKeyStore;
const keyStore = new UnencryptedFileSystemKeyStore(credentialsPath)

// Setup default client options
const options = {
  networkId: NETWORK_ID,
  nodeUrl: `https://rpc.${NETWORK_ID}.near.org`,
  walletUrl: `https://wallet.${NETWORK_ID}.near.org`,
  helperUrl: `https://helper.${NETWORK_ID}.near.org`,
  explorerUrl: `https://explorer.${NETWORK_ID}.near.org`,
  accountId: process.env.NEAR_ACCOUNT,
  deps: {
    keyStore: keyStore
  }
}

// Configure transaction details
const txSender = ACCOUNT_A;
const txReceiver = ACCOUNT_B;
const txAmount = formatAmount(1);

async function main() {
  // Configure the client with options and our local key store
  const client = await near.connect(options);
  const provider = client.connection.provider;

  // Private key configuration
  const keyRootPath = client.connection.signer.keyStore.keyDir;
  const keyFilePath = `${keyRootPath}/${options.networkId}/${ACCOUNT_A}.json`;

  // Load key pair from the file
  const content = JSON.parse(fs.readFileSync(keyFilePath).toString());
  const keyPair = near.KeyPair.fromString(content.private_key);

  // Get the sender public key
  const publicKey = keyPair.getPublicKey();
  console.log("Sender public key:", publicKey.toString())

  // Get the public key information from the node
  const accessKey = await provider.query(
    `access_key/${txSender}/${publicKey.toString()}`, ""
  );
  console.log("Sender access key:", accessKey);

  // GENERATE Temp Key
  const senderTempKeypair = KeyPair.fromRandom('ed25519');
  const tempPublicKey = senderTempKeypair.getPublicKey();
  
  console.log("Sender Temp Key:", tempPublicKey.toString(), senderTempKeypair.toString());
  // return;

  // Check to make sure provided key is a full access key
  if (accessKey.permission !== "FullAccess") {
    return console.log(`Account [${txSender}] does not have permission to send tokens using key: [${publicKey}]`);
  };

  // ----------------------------------------------
  // ADD KEY
  // ----------------------------------------------

  // Each transaction requires a unique number or nonce
  // This is created by taking the current nonce and incrementing it
  const nonce = ++accessKey.nonce;
  console.log("Calculated nonce:", nonce);

  // Construct actions that will be passed to the createTransaction method below
  const FullAccess = near.transactions.fullAccessKey()
  const actions = [near.transactions.addKey(senderTempKeypair.publicKey, FullAccess)];

  // Convert a recent block hash into an array of bytes.
  // This is required to prove the tx was recently constructed (within 24hrs)
  const recentBlockHash = near.utils.serialize.base_decode(accessKey.block_hash);
  console.log('accessKey.block_hash', accessKey.block_hash);

  // Create a new transaction object
  const transaction = near.transactions.createTransaction(
    txSender,
    publicKey,
    txSender,
    // txReceiver,
    nonce,
    actions,
    recentBlockHash
  );

  // Before we can sign the transaction we must perform three steps
  // 1) Serialize the transaction in Borsh
  const serializedTx = near.utils.serialize.serialize(
    near.transactions.SCHEMA,
    transaction
  );

  // 2) Hash the serialized transaction using sha256
  const serializedTxHash = new Uint8Array(sha256.array(serializedTx));

  // 3) Create a signature using the hashed transaction
  const signature = keyPair.sign(serializedTxHash);
  // console.log('signature', signature);

  // Sign the transaction
  const signedTransaction = new near.transactions.SignedTransaction({
    transaction,
    signature: new near.transactions.Signature({
      keyType: transaction.publicKey.keyType,
      data: signature.signature
    })
  });
  // console.log('signedTransaction', signedTransaction);

  // Send the transaction
  try {
    const result = await provider.sendTransaction(signedTransaction);

    console.log("Creation result:", result.transaction);
    console.log("----------------------------------------------------------------");
    console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
    console.log(`${options.explorerUrl}/transactions/${result.transaction.hash}`);
    console.log("----------------------------------------------------------------");

    setTimeout(async function () {
      console.log("Checking transaction status:", result.transaction.hash);

      const status = await provider.sendJsonRpc("tx", [result.transaction.hash, ACCOUNT_A]);
      // console.log("Transaction status:", status);
    }, 5000);
  }
  catch (error) {
    console.log("ERROR:", error);
  }
  // ----------------------------------------------
  // END ADD KEY
  // ----------------------------------------------

  // ----------------------------------------------
  // SEND MONEY
  // ----------------------------------------------

  // Each transaction requires a unique number or nonce
  // This is created by taking the current nonce and incrementing it
  const nonceDos = ++FullAccess.nonce;
  console.log("Calculated nonceDos:", nonceDos);

  // Construct actions that will be passed to the createTransaction method below
  const actionsDos = [near.transactions.transfer(txAmount)];

  // Convert a recent block hash into an array of bytes.
  // This is required to prove the tx was recently constructed (within 24hrs)
  const recentBlockHashDos = near.utils.serialize.base_decode(accessKey.block_hash);

  // Create a new transaction object
  const transactionDos = near.transactions.createTransaction(
    txSender,
    tempPublicKey,
    txReceiver,
    nonceDos,
    actionsDos,
    recentBlockHashDos
  );
  // console.log('transactionDos', transactionDos);

  // Before we can sign the transaction we must perform three steps
  // 1) Serialize the transaction in Borsh
  const serializedTxDos = near.utils.serialize.serialize(
    near.transactions.SCHEMA,
    transactionDos
  );
  // console.log('serializedTxDos', serializedTxDos);

  // 2) Hash the serialized transaction using sha256
  const serializedTxHashDos = new Uint8Array(sha256.array(serializedTxDos));

  // 3) Create a signature using the hashed transaction
  const signatureDos = senderTempKeypair.sign(serializedTxHashDos);

  // Sign the transaction
  const signedTransactionDos = new near.transactions.SignedTransaction({
    transaction: transactionDos,
    signature: new near.transactions.Signature({
      keyType: transactionDos.publicKey.keyType,
      data: signatureDos.signature
    })
  });

  // Send the transaction
  try {
    const resultDos = await provider.sendTransaction(signedTransactionDos);

    console.log("Creation result DOS:", resultDos.transaction);
    console.log("----------------------------------------------------------------");
    console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
    console.log(`${options.explorerUrl}/transactions/${resultDos.transaction.hash}`);
    console.log("----------------------------------------------------------------");

    setTimeout(async function () {
      console.log("Checking transaction status DOS:", resultDos.transaction.hash);

      const statusDos = await provider.sendJsonRpc("tx", [resultDos.transaction.hash, ACCOUNT_A]);
      // console.log("Transaction status DOS:", statusDos);
    }, 5000);
  }
  catch (e) {
    console.log("ERROR:", e);
  }
  // ----------------------------------------------
  // END SEND MONEY
  // ----------------------------------------------

  // ----------------------------------------------
  // DELETE KEY
  // ----------------------------------------------

  // Each transaction requires a unique number or nonce
  // This is created by taking the current nonce and incrementing it
  const nonceTres = ++accessKey.nonce;
  console.log("Calculated nonce tres:", nonceTres);

  // Construct actions that will be passed to the createTransaction method below
  const actionsTres = [near.transactions.deleteKey(senderTempKeypair.publicKey)];

  // Convert a recent block hash into an array of bytes.
  // This is required to prove the tx was recently constructed (within 24hrs)
  const recentBlockHashTres = near.utils.serialize.base_decode(accessKey.block_hash);

  // Create a new transaction object
  const transactionTres = near.transactions.createTransaction(
    txSender,
    publicKey,
    txSender,
    nonceTres,
    actionsTres,
    recentBlockHashTres
  );

  // Before we can sign the transaction we must perform three steps
  // 1) Serialize the transaction in Borsh
  const serializedTxTres = near.utils.serialize.serialize(
    near.transactions.SCHEMA,
    transactionTres
  );

  // 2) Hash the serialized transaction using sha256
  const serializedTxHashTres = new Uint8Array(sha256.array(serializedTxTres));

  // 3) Create a signature using the hashed transaction
  const signatureTres = keyPair.sign(serializedTxHashTres);
  // console.log('signature', signature);

  // Sign the transaction
  const signedTransactionTres = new near.transactions.SignedTransaction({
    transaction: transactionTres,
    signature: new near.transactions.Signature({
      keyType: transaction.publicKey.keyType,
      data: signatureTres.signature
    })
  });
  // console.log('signedTransaction', signedTransactionTres);

  // Send the transaction
  try {
    const resultTres = await provider.sendTransaction(signedTransactionTres);

    console.log("Creation resultTres:", resultTres.transaction);
    console.log("----------------------------------------------------------------");
    console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
    console.log(`${options.explorerUrl}/transactions/${resultTres.transaction.hash}`);
    console.log("----------------------------------------------------------------");

    setTimeout(async function () {
      console.log("Checking transaction statusTres:", resultTres.transaction.hash);

      const statusTres = await provider.sendJsonRpc("tx", [resultTres.transaction.hash, ACCOUNT_A]);
      // console.log("Transaction statusTres:", statusTres);
    }, 5000);
  }
  catch (ee) {
    console.log("ERROR:", ee);
  }
  // ----------------------------------------------
  // END DELETE KEY
  // ----------------------------------------------

  setTimeout(async () => {

    // ----------------------------------------------
    // ADD KEY
    // ----------------------------------------------

    // Each transaction requires a unique number or nonce
    // This is created by taking the current nonce and incrementing it
    const nonceQuad = ++accessKey.nonce;
    console.log("Calculated nonce quad:", nonce);

    // Construct actions that will be passed to the createTransaction method below
    // const FullAccess = near.transactions.fullAccessKey()
    const actionsQuad = [near.transactions.addKey(senderTempKeypair.publicKey, FullAccess)];

    // Convert a recent block hash into an array of bytes.
    // This is required to prove the tx was recently constructed (within 24hrs)
    const recentBlockHashQuad = near.utils.serialize.base_decode(accessKey.block_hash);

    // Create a new transaction object
    const transactionQuad = near.transactions.createTransaction(
      txSender,
      publicKey,
      txSender,
      nonceQuad,
      actionsQuad,
      recentBlockHashQuad
    );

    // Before we can sign the transaction we must perform three steps
    // 1) Serialize the transaction in Borsh
    const serializedTxQuad = near.utils.serialize.serialize(
      near.transactions.SCHEMA,
      transactionQuad
    );

    // 2) Hash the serialized transaction using sha256
    const serializedTxHashQuad = new Uint8Array(sha256.array(serializedTxQuad));

    // 3) Create a signature using the hashed transaction
    const signatureQuad = keyPair.sign(serializedTxHashQuad);
    // console.log('signature', signatureQuad);

    // Sign the transaction
    const signedTransactionQuad = new near.transactions.SignedTransaction({
      transaction: transactionQuad,
      signature: new near.transactions.Signature({
        keyType: transactionQuad.publicKey.keyType,
        data: signatureQuad.signature
      })
    });
    // console.log('signedTransaction', signedTransactionQuad);

    // Send the transaction
    try {
      const resultQuad = await provider.sendTransaction(signedTransactionQuad);

      console.log("Creation resultQuad:", resultQuad.transaction);
      console.log("----------------------------------------------------------------");
      console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
      console.log(`${options.explorerUrl}/transactions/${resultQuad.transaction.hash}`);
      console.log("----------------------------------------------------------------");

      setTimeout(async function () {
        console.log("Checking transaction statusQuad:", resultQuad.transaction.hash);

        const statusQuad = await provider.sendJsonRpc("tx", [resultQuad.transaction.hash, ACCOUNT_A]);
        // console.log("Transaction statusQuad:", statusQuad);
      }, 5000);
    }
    catch (eee) {
      console.log("ERROR:", eee);
    }
  // ----------------------------------------------
  // END ADD KEY
  // ----------------------------------------------
  }, 10000)

  // // ----------------------------------------------
  // // REPLAY
  // // ----------------------------------------------
  // // Send the transaction
  // try {
  //   const resultREPLAY = await provider.sendTransaction(signedTransactionDos);

  //   console.log("Creation result REPLAY:", resultREPLAY.transaction);
  //   console.log("----------------------------------------------------------------");
  //   console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
  //   console.log(`${options.explorerUrl}/transactions/${resultREPLAY.transaction.hash}`);
  //   console.log("----------------------------------------------------------------");

  //   setTimeout(async function () {
  //     console.log("Checking transaction status REPLAY:", resultREPLAY.transaction.hash);

  //     const statusREPLAY = await provider.sendJsonRpc("tx", [resultREPLAY.transaction.hash, ACCOUNT_A]);
  //     // console.log("Transaction status REPLAY:", statusREPLAY);
  //   }, 5000);
  // }
  // catch (eeeee) {
  //   console.log("ERROR:", eeeee);
  // }

  setTimeout(async () => {
    // ----------------------------------------------
    // SEND MOAR MONEY
    // ----------------------------------------------

    // Each transaction requires a unique number or nonce
    // This is created by taking the current nonce and incrementing it
    // const nonceSync = FullAccess.nonce;
    // console.log("Calculated nonceDos:", nonceDos);

    // // Construct actions that will be passed to the createTransaction method below
    // const actionsSync = [near.transactions.transfer(txAmount)];

    // Get the public key information from the node
    const accessKeyMoar = await provider.query(
      `access_key/${txSender}/${tempPublicKey.toString()}`, ""
    );
    console.log('accessKeyMoar', accessKeyMoar);

    // Convert a recent block hash into an array of bytes.
    // This is required to prove the tx was recently constructed (within 24hrs)
    const recentBlockHashMoar = near.utils.serialize.base_decode(accessKeyMoar.block_hash);
    console.log('accessKeyMoar.block_hash', accessKeyMoar.block_hash);

    // Create a new transaction object
    const transactionMoar = near.transactions.createTransaction(
      txSender,
      tempPublicKey,
      txReceiver,
      nonceDos,
      actionsDos,
      recentBlockHashMoar
    );
    // console.log('transactionDos', transactionDos);

    // Before we can sign the transaction we must perform three steps
    // 1) Serialize the transaction in Borsh
    const serializedTxMoar = near.utils.serialize.serialize(
      near.transactions.SCHEMA,
      transactionMoar
    );
    // console.log('serializedTxDos', serializedTxDos);

    // 2) Hash the serialized transaction using sha256
    const serializedTxHashMoar = new Uint8Array(sha256.array(serializedTxMoar));

    // 3) Create a signature using the hashed transaction
    const signatureMoar = senderTempKeypair.sign(serializedTxHashMoar);

    // Sign the transaction
    const signedTransactionMoar = new near.transactions.SignedTransaction({
      transaction: transactionMoar,
      signature: new near.transactions.Signature({
        keyType: transactionMoar.publicKey.keyType,
        data: signatureMoar.signature
      })
    });

    // Send the transaction
    try {
      const resultMoar = await provider.sendTransaction(signedTransactionMoar);

      console.log("Creation result DOS:", resultDos.transaction);
      console.log("----------------------------------------------------------------");
      console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
      console.log(`${options.explorerUrl}/transactions/${resultMoar.transaction.hash}`);
      console.log("----------------------------------------------------------------");

      setTimeout(async function () {
        console.log("Checking transaction status DOS:", resultMoar.transaction.hash);

        // const statusMoar = await provider.sendJsonRpc("tx", [resultMoar.transaction.hash, ACCOUNT_A]);
        // console.log("Transaction status DOS:", statusDos);
      }, 5000);
    }
    catch (e) {
      console.log("ERROR:", e);
    }
  // ----------------------------------------------
  // END SEND MOAR MONEY
  // ----------------------------------------------
  }, 20000)
};

main()
