const { connect, KeyPair, keyStores, utils } = require('near-api-js');
const fs = require('fs');
const path = require('path');
const homedir = require('os').homedir();

const CREDENTIALS_DIR = '.near-credentials';
const credentialsPath = path.join(homedir, CREDENTIALS_DIR);

const accounts = [
  {
    id: 'tdev_v0',
    // if this doesnt exist, it _should_ find or generate
    idPublicKey: 'ed25519:5D5T7hvFyjF5AFDGVPK5upUVmwDLojWsZMUHBJ22oi8t',
    alias: [['tdev_v001', '0.021'], ['tdev_v002', '0.022']],
    // subAccounts: [['c', '1'], ['d', '1.336']], // almost leet, a hash can dream #AmIRite?
  },
]

const nearConfig = {
  networkId: 'testnet',
  networkName: 'testnet',
  nodeUrl: 'https://rpc.testnet.near.org',
  walletUrl: 'https://wallet.testnet.near.org',
  helperUrl: 'https://helper.testnet.near.org'
};

const getAccountNameId = str => `${str}.${nearConfig.networkName}`

let nearClient
const getNearConnection = async () => {
  if (nearClient) return nearClient
  const keyStore = new keyStores.UnencryptedFileSystemKeyStore(credentialsPath);
  nearClient = await connect(Object.assign({ deps: { keyStore } }, nearConfig));
  return nearClient
}

const accountGoBrrr = async ({ account_id, new_account_id, new_public_key, initialBalance }) => {
  const near = await getNearConnection()
  const creatorAccount = await near.account(account_id);

  return creatorAccount.functionCall(
    nearConfig.networkName,
    'create_account',
    { new_account_id, new_public_key },
    '300000000000000', // buy some gas
    utils.format.parseNearAmount(initialBalance) /// SOOOO cheaprs
  )
}

const genNewAccount = async (accountId, nearConfig) => {
  const keyStore = new keyStores.UnencryptedFileSystemKeyStore(credentialsPath);

  const existingKey = await keyStore.getKey(nearConfig.networkId, accountId);
  if (existingKey) {
    console.log(`Key pair with ${existingKey.publicKey} public key for an account "${accountId}"`);
    return existingKey.publicKey;
  }

  const keyPair = KeyPair.fromRandom('ed25519');
  const publicKey = keyPair.publicKey.toString();
  const id = accountId || implicitAccountId(publicKey);
  await keyStore.setKey(nearConfig.networkId, id, keyPair);
  console.log(`Key pair with ${publicKey} public key for an account "${id}"`);
  return publicKey;
}

// top level account
// check if have wallet for it, then create
const heplMeHodl = async account => {
  // Check local account
  const file = `${account.id}.${nearConfig.networkId}.json`
  const hasDefaultJson = await fs.existsSync(path.join(credentialsPath, 'default', file))
  const hasNetworkJson = await fs.existsSync(path.join(credentialsPath, nearConfig.networkId, file))

  // if no wallet, check if it exists on network
  if (!hasDefaultJson && !hasNetworkJson) {
    // account.idPublicKey = await genNewAccount(`${account.id}.${nearConfig.networkId}`, nearConfig)
    console.warn(`unable to create and fund new top level accounts ${account.id}.${nearConfig.networkId}`)
  }

  await shogtunHodlr(account)

  // TODO: Create subAccounts method
}

// alias top level account
const shogtunHodlr = async account => {
  if (account.alias && account.alias.length > 0) {
    for (const alias of account.alias) {
      await accountGoBrrr({
        account_id: getAccountNameId(account.id),
        new_account_id: getAccountNameId(alias[0]),
        new_public_key: account.idPublicKey,
        initialBalance: alias[1] || '0.02',
      })

      console.log('Alias Created:', getAccountNameId(alias[0]))
    }
  }
}

;(async () => {
  // cereal, needs 04T MIKL
  for (const acct of accounts) {
    await heplMeHodl(acct)
  }
})();
